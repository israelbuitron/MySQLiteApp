package org.israelbuitron.demos.mysqliteapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.israelbuitron.demos.mysqliteapp.R;
import org.israelbuitron.demos.mysqliteapp.adapters.MyAdapter;
import org.israelbuitron.demos.mysqliteapp.database.BookSQLiteDbHelper;
import org.israelbuitron.demos.mysqliteapp.models.Book;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * Debug tag.
     */
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Database helper.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private BookSQLiteDbHelper mDbHelper;

    /**
     * Recycler view for activity
     */
    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView mRecyclerView;

    /**
     * Adapter for actitivy's recycler view
     */
    @SuppressWarnings("FieldCanBeLocal")
    private MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get a database helper
        mDbHelper = new BookSQLiteDbHelper(this);
        Log.d(TAG, "Database helper created.");

/*
        // FIXME: Change this dummy preload data
        List<Book> defaultBooks = new ArrayList<>();
        defaultBooks.add(new Book(1, "Diario de un mal año", 2005, "John Maxwell Coetzee"));
        defaultBooks.add(new Book(2, "Why nations fail", 2013, "Daron Acemoglu"));
        defaultBooks.add(new Book(3, "The God Delusion", 2008, "Richard Dawkins"));
        defaultBooks.add(new Book(4, "The selfish gene", 2006, "Richard Dawkins"));
        saveBooks(defaultBooks);
*/

        // Load books from database
        List<Book> books = mDbHelper.loadBooks();
        Log.d(TAG, books.size() + "books loaded from database.");

        // Get recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // Fill adapter with books data
        mAdapter = new MyAdapter(books);

        // Set adapter for recycler view
        mRecyclerView.setAdapter(mAdapter);
    }
}
