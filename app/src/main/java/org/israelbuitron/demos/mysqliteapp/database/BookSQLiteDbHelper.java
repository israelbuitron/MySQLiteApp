package org.israelbuitron.demos.mysqliteapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.israelbuitron.demos.mysqliteapp.models.Book;

import java.util.ArrayList;
import java.util.List;

public class BookSQLiteDbHelper extends SQLiteOpenHelper {

    /**
     * Debug tag.
     */
    private static final String TAG = BookSQLiteDbHelper.class.getSimpleName();

    /**
     * Database filename.
     */
	private static final String DATABASE_NAME = "Books.db";

    /**
     * Database version.
     */
    private static final int DATABASE_VERSION = 1;


    public BookSQLiteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        // Log books table creation
        Log.d(TAG, "Database [" + DATABASE_NAME + "], version [" + DATABASE_VERSION + "] is used.");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Execute books table creation callback
    	db.execSQL(BookContract.SQL_CREATE_BOOKS_TABLE);

        // Log books table creation
        Log.d(TAG, "Table [" + BookContract.BookEntry.TABLE_NAME + "] for database version [" + DATABASE_VERSION + "] created.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Log books table update callback
        Log.d(TAG, "Database update executed.");

        // Execute books table drop
		db.execSQL(BookContract.SQL_DELETE_BOOKS_TABLE);

        // Log books table drop
        Log.d(TAG, "Table [" + BookContract.BookEntry.TABLE_NAME + "] for database version [" + DATABASE_VERSION + "] dropped.");

        onCreate(db);
    }

    @SuppressWarnings("unused")
    public void saveBooks(List<Book> books) {
        // Gets database in write mode
        SQLiteDatabase db = getWritableDatabase();

        // Insert each book into database
        for (Book b : books) {
            // Create a map of values for new books
            ContentValues values = new ContentValues();
            values.put(BookContract.BookEntry.COLUMN_NAME_ENTRY_ID, b.getId());
            values.put(BookContract.BookEntry.COLUMN_NAME_TITLE, b.getTitle());
            values.put(BookContract.BookEntry.COLUMN_NAME_AUTHOR, b.getAuthor());
            values.put(BookContract.BookEntry.COLUMN_NAME_YEAR, b.getYear());

            // Insert the new row, returning the primary key value of the new row
            long newRowId = db.insert(BookContract.BookEntry.TABLE_NAME, null, values);

            // Print to debug book inserted into books table
            Log.d(TAG, "Book inserted with id " + newRowId);
        }

        // Close connection to database
        db.close();
    }

    public List<Book> loadBooks() {
        // Gets database in read mode
        SQLiteDatabase db = getReadableDatabase();

        // Set projection for query
        String[] projection = {
                BookContract.BookEntry.COLUMN_NAME_ENTRY_ID,
                BookContract.BookEntry.COLUMN_NAME_TITLE,
                BookContract.BookEntry.COLUMN_NAME_YEAR,
                BookContract.BookEntry.COLUMN_NAME_AUTHOR,
        };

        // Set selection for query results
        //String whereClause = "";

        // Set sort for query results
        String orderClause = BookContract.BookEntry.COLUMN_NAME_YEAR + BookContract.DESCENDENT_SORT;

        // Execute query and retrieve a cursor pointer to results
        Cursor c = db.query(
                BookContract.BookEntry.TABLE_NAME, // Set FROM clause, table to query
                projection,                        // Set SELECT clause columns to return
                null,                              // Set WHERE clause columns
                null,                              // Set WHERE clause values
                null,                              // don't group the rows
                null,                              // Set GROUP BY filter by row groups
                orderClause                        // Set ORDER BY criteria
        );

        // Ensure to iterate query results from beginning
        c.moveToFirst();

        // Create list of books to return
        List<Book> books = new ArrayList<>();

        // Process each row from query results
        while(c.moveToNext()) {
            // Create a Book object from data retrieved from query
            Book book = new Book(c.getInt(0), c.getString(1), c.getInt(2), c.getString(3) );

            // Print to debug book retrieved from SQL query
            Log.d(TAG, book.toString());

            // Add book to list of books
            books.add(book);
        }

        // Close connection to database
        c.close();
        db.close();

        // Return books
        return books;
    }

}
