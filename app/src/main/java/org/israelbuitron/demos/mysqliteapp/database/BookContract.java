package org.israelbuitron.demos.mysqliteapp.database;

import android.provider.BaseColumns;
import org.israelbuitron.demos.mysqliteapp.models.Book;

/**
 */
public class BookContract {
    /**
     * This class defines table content.
     */
    public static abstract class BookEntry implements BaseColumns {
        /**
         * Table name of {@link Book} model.
         */
        public static final String TABLE_NAME = "books";

        /**
         * Column name for {@link Book} id.
         */
        public static final String COLUMN_NAME_ENTRY_ID = "id";

        /**
         * Column name for {@link Book} title.
         */
        public static final String COLUMN_NAME_TITLE = "title";

        /**
         * Column name for {@link Book} publication year.
         */
        public static final String COLUMN_NAME_YEAR = "year";

        /**
         * Column name for {@link Book} author.
         */
        public static final String COLUMN_NAME_AUTHOR = "author";

    }

    /**
     * Key string for primary key.
     */
    public static final String PRIMARY_KEY_TYPE = " PRIMARY KEY";

    /**
     * Key string for date datatype.
     */
    public static final String DATE_TYPE = " DATE";

    /**
     * Key string for text datatype.
     */
    public static final String TEXT_TYPE = " TEXT";

    /**
     * Key string for NOT NULL column constraint.
     */
    public static final String NOT_NULL_TYPE = " NOT NULL";

    /**
     * Key string for descendent sort.
     */
    public static final String DESCENDENT_SORT = " DESC";

    /**
     * SQL statement to create table for {@link Book} model.
     */
    public static final String SQL_CREATE_BOOKS_TABLE =
            "CREATE TABLE " + BookEntry.TABLE_NAME + "(" +
                    BookEntry.COLUMN_NAME_ENTRY_ID + PRIMARY_KEY_TYPE + "," +
                    BookEntry.COLUMN_NAME_TITLE  + TEXT_TYPE + NOT_NULL_TYPE + "," +
                    BookEntry.COLUMN_NAME_YEAR   + DATE_TYPE + NOT_NULL_TYPE + "," +
                    BookEntry.COLUMN_NAME_AUTHOR + TEXT_TYPE + NOT_NULL_TYPE +
                    ")";

    /**
     * SQL statement to drop table for {@link Book} model.
     */
    public static final String SQL_DELETE_BOOKS_TABLE =
            "DROP TABLE IF EXISTS " + BookEntry.TABLE_NAME;
}

