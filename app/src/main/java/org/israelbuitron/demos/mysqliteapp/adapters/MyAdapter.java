package org.israelbuitron.demos.mysqliteapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.israelbuitron.demos.mysqliteapp.R;
import org.israelbuitron.demos.mysqliteapp.models.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a {@link RecyclerView.Adapter} for a list filled with {@link Book} models.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    /**
     * Debug tag.
     */
    private static final String TAG = MyAdapter.class.getSimpleName();

    private List<Book> mBooks;

    /**
     * Creates an instance with an empty collection of {@link Book} models.
     */
    @SuppressWarnings("unused")
    public MyAdapter() {
        mBooks = new ArrayList<>();
        Log.d(TAG, "Adapter with no data created");
    }

    /**
     * Creates an instance with a collection of {@link Book} models.
     */
    public MyAdapter(List<Book> books) {
        mBooks = books;
        Log.d(TAG, "Adapter with "+ books.size() +" books created");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.book_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Call view holder to fill book data into views
        holder.setBook(mBooks.get(position));
    }

    @Override
    public int getItemCount() {
        return mBooks.size();
    }

    @SuppressWarnings("unused")
    public void setBooks(List<Book> books) {
        mBooks = books;
        notifyDataSetChanged();
    }

    /**
     * {@link RecyclerView.ViewHolder} implementation for {@link Book} model.
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        /**
         * {@link Book} model to display
         */
        public Book mBook;

        /**
         * View for book title.
         */
        public TextView mTitle;

        /**
         * View for book author.
         */
        public TextView mAuthor;

        /**
         * View for book year.
         */
        public TextView mYear;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.title_view);
            mAuthor = (TextView) itemView.findViewById(R.id.author_view);
            mYear = (TextView) itemView.findViewById(R.id.year_view);
        }

        /**
         * Set a {@link Book} model to view holder.
         */
        public void setBook(Book book) {
            mBook = book;
            mTitle.setText(mBook.getTitle());
            mAuthor.setText(mBook.getAuthor());
            mYear.setText(mBook.getYear().toString());
        }
    }
}
