package org.israelbuitron.demos.mysqliteapp.models;

/**
 * Book model class.
 */
@SuppressWarnings("unused")
public class Book {
    /**
     * {@link Book} id.
     */
    private Integer mId;

    /**
     * {@link Book} title.
     */
    private String mTitle;

    /**
     * {@link Book} publication year.
     */
    private Integer mYear;

    /**
     * {@link Book} author.
     */
    private String mAuthor;

    /**
     * Creates a new instances with initial attribute values.
     * @param id Book id.
     * @param title Book title.
     * @param year Book year.
     * @param author Book author.
     */
    public Book(Integer id, String title, Integer year, String author) {
        mId = id;
        mTitle = title;
        mYear = year;
        mAuthor = author;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Integer getYear() {
        return mYear;
    }

    public void setYear(Integer year) {
        mYear = year;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "mId=" + mId +
                ", mTitle='" + mTitle + '\'' +
                ", mYear=" + mYear +
                ", mAuthor='" + mAuthor + '\'' +
                '}';
    }
}
